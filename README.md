
# Banking mock mobile application for iOS #

This project is the first step to create mobile banking app. For now, it includes the basic transfer money between two users.

### How to setup the project ###

* clone the project
* run ```pod install``` on project root folder
* run the project in XCode


### User Accounts ###
Please login using the following accounts for testing.
#### Bob ####
***email*** : bob@email.com

***password*** : Test1234

#### Alice ####
***email*** : alice@email.com

***password*** : Test1234


### Recommendation ###

 **Firebase**: This project is using Firebase Authentication and Firebase Firestore. Therefore, if you change the bundle id, there can have some issue to run the project. To avoid the issue, please run with simulator without changing bundle id.
