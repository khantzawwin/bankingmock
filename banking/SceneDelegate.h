//
//  SceneDelegate.h
//  banking
//
//  Created by Khant Zaw Win on 15/5/21.
//

#import <UIKit/UIKit.h>
#import <FirebaseAuth.h>

@interface SceneDelegate : UIResponder <UIWindowSceneDelegate>

@property (strong, nonatomic) UIWindow * window;
@property (strong, nonatomic) FIRUser *firebaseUser;
@property (strong, nonatomic) NSDictionary *dbUser;


@end

