//
//  TopupViewController.h
//  banking
//
//  Created by Khant Zaw Win on 16/5/21.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN
@protocol TopupViewControllerDelegate <NSObject>
@optional

- (void)reloadDataWithMessage:(NSString *)message;

@end

@interface TopupViewController : UIViewController

@property(strong, nonatomic) NSDictionary *dbUser;
@property (nonatomic, weak) id <TopupViewControllerDelegate> delegate;

@end

NS_ASSUME_NONNULL_END
