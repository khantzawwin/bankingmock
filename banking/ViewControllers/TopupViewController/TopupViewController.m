//
//  TopupViewController.m
//  banking
//
//  Created by Khant Zaw Win on 16/5/21.
//

#import "TopupViewController.h"
#import "FireStoreHelper.h"
#import "Helper.h"

@interface TopupViewController () <UITextFieldDelegate,FireStoreHelperDelegate>
@property(weak, nonatomic) IBOutlet UITextField *amountTextField;
@property(strong, nonatomic) NSString *message;

@end

@implementation TopupViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.amountTextField.delegate = self;
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissKeyboard)];
    [self.view addGestureRecognizer:tap];
}

-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    [UIView animateWithDuration:.3 animations:^{
        self.view.frame = CGRectMake(self.view.frame.origin.x, self.view.frame.origin.y -200., self.view.frame.size.width, self.view.frame.size.height);
    }];
}


-(void)textFieldDidEndEditing:(UITextField *)textField
{
    [UIView animateWithDuration:.3 animations:^{
        self.view.frame = CGRectMake(self.view.frame.origin.x, self.view.frame.origin.y +200., self.view.frame.size.width, self.view.frame.size.height);
    }];
}

-(void)dismissKeyboard
{
    [self.amountTextField resignFirstResponder];
}

- (IBAction)onClickTopup:(id)sender{
    
    double topay = [self.dbUser[@"topay"] doubleValue];
    
    if(topay > 0)
    {
        NSString *email = @"";
        
        if([self.dbUser[@"email"] isEqualToString:@"bob@email.com"]){
            email = @"alice@email.com";
        }
        else{
            email = @"bob@email.com";
        }
        
        [[FireStoreHelper sharedInstance]getUserByEmail:email withDelegate:self];
    }
    else
    {
        //no debt. Top up balance
        double current_balance = [self.dbUser[@"balance"] doubleValue];
        double topup_value = [self.amountTextField.text doubleValue];
        NSNumber * total_balance = [[NSNumber alloc]initWithDouble:(current_balance+topup_value)];
        [self.dbUser setValue:total_balance forKey:@"balance"];
        [[FireStoreHelper sharedInstance] addOrUpdateUserWithEmail:self.dbUser[@"email"] user:self.dbUser andDelegate:self];
    }
}

- (void)getUserInfo: (NSDictionary *)userInfo{
    if(userInfo)
    {
        double sender_balance = [self.dbUser[@"balance"] doubleValue];
        double sender_topay = [self.dbUser[@"topay"] doubleValue];
        double recipient_balance = [userInfo[@"balance"] doubleValue];
        double amount = [self.amountTextField.text doubleValue];
        
        if (amount >= sender_topay)
        {
            sender_balance = amount - sender_topay;
            recipient_balance = recipient_balance + sender_topay;
            
            self.message = [NSString stringWithFormat:@"Transferred %.2f to %@.",sender_topay,[userInfo objectForKey:@"name"]];
            
            sender_topay = 0;
        }
        else
        {
            sender_topay = sender_topay - amount;
            recipient_balance = recipient_balance + amount;
            
            self.message = [NSString stringWithFormat:@"Transferred %.2f to %@.",amount,[userInfo objectForKey:@"name"]];
        }
        
        [self.dbUser setValue:[[NSNumber alloc] initWithDouble:sender_balance] forKey:@"balance"];
        [self.dbUser setValue:[[NSNumber alloc] initWithDouble:sender_topay] forKey:@"topay"];
        [userInfo setValue:[[NSNumber alloc] initWithDouble:recipient_balance] forKey:@"balance"];
        
        [[FireStoreHelper sharedInstance] makeTransactionWithSender:self.dbUser AndRecipient:userInfo AndDelegate:self];
    }
    else
    {
        [Helper ShowErrorMsgDialogAlert:@"Recipient email not found." andView:self];
    }
}

- (void)addOrUpdateUserSuccess{
    [[self presentingViewController] dismissViewControllerAnimated:YES completion:nil];
    [self.delegate reloadDataWithMessage:self.message];
}

- (void)errorWithMessage:(NSError *)message{
    [Helper ShowErrorMsgDialogAlert:message.localizedDescription andView:self];
}

- (void)transactionSuccess{
    [[self presentingViewController] dismissViewControllerAnimated:YES completion:nil];
    [self.delegate reloadDataWithMessage:self.message];
}

@end
