//
//  LoginViewController.m
//  banking
//
//  Created by Khant Zaw Win on 15/5/21.
//

#import "LoginViewController.h"
#import <FirebaseAuth.h>
#import "AppDelegate.h"
#import "Helper.h"

@interface LoginViewController ()

@property(weak, nonatomic) IBOutlet UITextField *nameTextField;
@property(weak, nonatomic) IBOutlet UITextField *emailTextField;
@property(weak, nonatomic) IBOutlet UITextField *passwordTextField;
@property(weak, nonatomic) IBOutlet UILabel *createAccountLabel;
@property(weak, nonatomic) IBOutlet UILabel *loginLabel;
@property(weak, nonatomic) IBOutlet UIButton *loginButton;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *emailToLogInConstraint;

@property (nonatomic, assign) BOOL isLogIn;

@end

@implementation LoginViewController



- (void)viewDidLoad {
    [super viewDidLoad];
    self.isLogIn = YES;
    // Do any additional setup after loading the view from its nib.
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissKeyboard)];
    [self.view addGestureRecognizer:tap];
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self switchUI];
    
    //Add forget passwrd Function
    UITapGestureRecognizer *createAccountGesture = [[UITapGestureRecognizer alloc] initWithTarget:self
                                                                                     action:@selector(createAccountOnClick:)];
    [self.createAccountLabel addGestureRecognizer:createAccountGesture];
}


- (void)switchUI{
    if (self.isLogIn)
    {
        self.emailToLogInConstraint.constant = 5;
        [self.nameTextField setHidden:YES];
        
        //Underline
        NSMutableAttributedString *attributeString = [[NSMutableAttributedString alloc] initWithString:@"Don't have an account? Sign up here."];
        [attributeString addAttribute:NSUnderlineStyleAttributeName
                                value:[NSNumber numberWithInt:1]
                                range:(NSRange){0,[attributeString length]}];
        
        [self.createAccountLabel setAttributedText:attributeString];
        
        [self.loginLabel setText:@"Log In"];
        [self.loginButton setTitle:@"Log In" forState:UIControlStateNormal];
    }
    else
    {
        self.emailToLogInConstraint.constant = 60;
        [self.nameTextField setHidden:NO];
        
        //Underline
        NSMutableAttributedString *attributeString = [[NSMutableAttributedString alloc] initWithString:@"Already have an account? Log in here."];
        [attributeString addAttribute:NSUnderlineStyleAttributeName
                                value:[NSNumber numberWithInt:1]
                                range:(NSRange){0,[attributeString length]}];
        
        [self.createAccountLabel setAttributedText:attributeString];
        
        [self.loginLabel setText:@"Sign Up"];
        [self.loginButton setTitle:@"Sign Up" forState:UIControlStateNormal];
    }
}

- (IBAction)onClickSignIn:(id)sender{
    if (self.isLogIn)
    {
        if ([self.emailTextField.text isEqualToString:@"bob@email.com"] || [self.emailTextField.text isEqualToString:@"alice@email.com"])
        {
            [[FIRAuth auth] signInWithEmail:self.emailTextField.text
                                   password:self.passwordTextField.text
                                 completion:^(FIRAuthDataResult * _Nullable authResult,
                                              NSError * _Nullable error) {
                if(error)
                {
                    [Helper ShowErrorMsgDialogAlert:error.localizedDescription andView:self];
                }
            }];
        }
        else
        {
            [Helper ShowErrorMsgDialogAlert:@"Please only use Bob or Alice email." andView:self];
        }
    }
    else
    {
        if(self.nameTextField.text.length)
        {
            [[FIRAuth auth] createUserWithEmail:self.emailTextField.text
                                       password:self.passwordTextField.text
                                     completion:^(FIRAuthDataResult * _Nullable authResult,
                                                  NSError * _Nullable error) {
                if(error)
                {
                    [Helper ShowErrorMsgDialogAlert:error.localizedDescription andView:self];
                }
                [AppDelegate instance].setUserName = self.nameTextField.text;
            }];
        }
        else
        {
            [Helper ShowErrorMsgDialogAlert:@"Please enter username." andView:self];
        }
    }
}

- (void)createAccountOnClick:(UITapGestureRecognizer *)recognizer {
    self.isLogIn = !self.isLogIn;
    [self switchUI];
}

-(void)dismissKeyboard
{
    [self.nameTextField resignFirstResponder];
    [self.emailTextField resignFirstResponder];
    [self.passwordTextField resignFirstResponder];
}

@end
