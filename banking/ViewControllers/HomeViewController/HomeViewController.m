//
//  HomeViewController.m
//  banking
//
//  Created by Khant Zaw Win on 16/5/21.
//

#import "HomeViewController.h"
#import <FirebaseAuth.h>
#import "FireStoreHelper.h"
#import "TopupViewController.h"
#import "PayViewController.h"
#import "Helper.h"

@interface HomeViewController () <FireStoreHelperDelegate,TopupViewControllerDelegate, PayViewControllerDelegate>

@property(weak, nonatomic) IBOutlet UILabel *nameLabel;
@property(weak, nonatomic) IBOutlet UILabel *balanceLabel;
@property(strong, nonatomic) NSDictionary *dbUser;

@end

@implementation HomeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
}

- (void) viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    [[FireStoreHelper sharedInstance] getUserswithDelegate:self];
    
}

- (void)getUsers:(NSArray *)users{
    NSString * email = [FIRAuth auth].currentUser.email;
    NSDictionary * current_user = [[NSDictionary alloc]init];
    NSDictionary * another_user = [[NSDictionary alloc]init];
    for (FIRDocumentSnapshot * user in users)
    {
        if ([user[@"email"] isEqualToString:email]){
            current_user = user.data;
        }
        else
        {
            another_user = user.data;
        }
    }
    
    self.nameLabel.text = [NSString stringWithFormat:@"Hello, %@",current_user[@"name"]];
    self.dbUser = current_user;
    double balance = [current_user[@"balance"] doubleValue];
    double debt = [current_user[@"topay"] doubleValue];
    double another_debt = [another_user [@"topay"] doubleValue];
    
    NSString * message = @"";
    NSString * another_user_name = another_user[@"name"];
    if (debt > 0)
    {
        message = [NSString stringWithFormat:@"Your Balance is %.2f. \nOwing %.2f to %@.", balance, debt, another_user_name];
    }
    else if (another_debt > 0)
    {
        message = [NSString stringWithFormat:@"Your Balance is %.2f. \nOwing %.2f from %@.", balance, another_debt, another_user_name];
    }
    else
    {
        message = [NSString stringWithFormat:@"Your Balance is %.2f.", balance];
    }
    
    self.balanceLabel.text = message;
    
}


- (IBAction)onClickLogout:(id)sender{
    [[FIRAuth auth] signOut:nil];
}

- (IBAction)onClickTopup:(id)sender{
    TopupViewController * vc = [[TopupViewController alloc]init];
    vc.delegate = self;
    vc.dbUser = self.dbUser;
    [self presentViewController:vc animated:YES completion:^{
        
    }];
}

- (IBAction)onClickPay:(id)sender{
    PayViewController * vc = [[PayViewController alloc]init];
    vc.delegate = self;
    vc.dbUser = self.dbUser;
    [self presentViewController:vc animated:YES completion:^{
        
    }];
}

-(void)reloadDataWithMessage:(NSString *)message{
    [[FireStoreHelper sharedInstance] getUserswithDelegate:self];
    if(message && ![message isEqualToString:@""])
        [Helper ShowErrorMsgDialogAlert:message andView:self];
}

- (void)errorWithMessage:(NSError *)message{
    [Helper ShowErrorMsgDialogAlert:message.localizedDescription andView:self];
}

@end
