//
//  PayViewController.m
//  banking
//
//  Created by Khant Zaw Win on 16/5/21.
//

#import "PayViewController.h"
#import "FireStoreHelper.h"
#import "Helper.h"

@interface PayViewController () <UITextFieldDelegate,FireStoreHelperDelegate>

@property(weak, nonatomic) IBOutlet UITextField *recipientTextField;
@property(weak, nonatomic) IBOutlet UITextField *amountTextField;
@property(strong, nonatomic) NSString *message;

@end

@implementation PayViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.recipientTextField.delegate = self;
    self.amountTextField.delegate = self;
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissKeyboard)];
    [self.view addGestureRecognizer:tap];
    
    if([self.dbUser[@"email"] isEqualToString:@"bob@email.com"])
    {
        self.recipientTextField.text = @"alice@email.com";
        [self.recipientTextField setEnabled:NO];
        self.recipientTextField.textColor = [UIColor lightGrayColor];
    }
    else
    {
        self.recipientTextField.text = @"bob@email.com";
        [self.recipientTextField setEnabled:NO];
        self.recipientTextField.textColor = [UIColor lightGrayColor];
    }
    
}

-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    [UIView animateWithDuration:.3 animations:^{
        self.view.frame = CGRectMake(self.view.frame.origin.x, self.view.frame.origin.y -200., self.view.frame.size.width, self.view.frame.size.height);
    }];
}


-(void)textFieldDidEndEditing:(UITextField *)textField
{
    [UIView animateWithDuration:.3 animations:^{
        self.view.frame = CGRectMake(self.view.frame.origin.x, self.view.frame.origin.y +200., self.view.frame.size.width, self.view.frame.size.height);
    }];
}

-(void)dismissKeyboard
{
    [self.recipientTextField resignFirstResponder];
    [self.amountTextField resignFirstResponder];
}

- (IBAction)onClickPay:(id)sender{
    if(self.recipientTextField.text.length && self.amountTextField.text.length)
        [[FireStoreHelper sharedInstance]getUserByEmail:self.recipientTextField.text withDelegate:self];
    else
        [Helper ShowErrorMsgDialogAlert:@"Please enter recipient email and amount." andView:self];
}

-(void) getUserInfo:(NSDictionary *)userInfo{
    if(userInfo)
    {
        double pay_amount = [self.amountTextField.text doubleValue];
        double sender_balance = [self.dbUser[@"balance"] doubleValue];
        double sender_topay = [self.dbUser[@"topay"] doubleValue];
        double recipient_balance = [userInfo[@"balance"] doubleValue];
        double recipient_topay = [userInfo[@"topay"] doubleValue];
        
        if (recipient_topay > 0)
        {
            if (pay_amount > recipient_topay)
            {
                double excuss = pay_amount - recipient_topay;
                
                recipient_topay = 0;
                recipient_balance = excuss;
                
                if ( excuss > sender_balance)
                {
                    sender_topay = -1 * (sender_balance - excuss);
                    sender_balance = 0;
                }
                else
                {
                    sender_balance = sender_balance - excuss;
                }
            }
            else
            {
                recipient_topay = recipient_topay - pay_amount;
            }
        }
        else
        {
            if (pay_amount > sender_balance)
            {
                recipient_balance = recipient_balance + sender_balance;
                sender_topay = -1 * (sender_balance - pay_amount);
                sender_balance = 0;
            }
            else
            {
                recipient_balance = recipient_balance + pay_amount;
                sender_balance = sender_balance - pay_amount;
            }
        }
        
        [self.dbUser setValue:[[NSNumber alloc] initWithDouble:sender_balance] forKey:@"balance"];
        [self.dbUser setValue:[[NSNumber alloc] initWithDouble:sender_topay] forKey:@"topay"];
        [userInfo setValue:[[NSNumber alloc] initWithDouble:recipient_balance] forKey:@"balance"];
        [userInfo setValue:[[NSNumber alloc] initWithDouble:recipient_topay] forKey:@"topay"];
        
        [[FireStoreHelper sharedInstance] makeTransactionWithSender:self.dbUser AndRecipient:userInfo AndDelegate:self];
        
        self.message = [NSString stringWithFormat:@"Transferred %.2f to %@.",pay_amount,[userInfo objectForKey:@"name"]];
        
    }
    else
    {
        [Helper ShowErrorMsgDialogAlert:@"Recipient email not found." andView:self];
    }
}

- (void)transactionSuccess{
    [[self presentingViewController] dismissViewControllerAnimated:YES completion:nil];
    [self.delegate reloadDataWithMessage:self.message];
}

- (void)errorWithMessage:(NSError *)message{
    [Helper ShowErrorMsgDialogAlert:message.localizedDescription andView:self];
}

@end
