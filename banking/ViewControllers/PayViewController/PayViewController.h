//
//  PayViewController.h
//  banking
//
//  Created by Khant Zaw Win on 16/5/21.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN
@protocol PayViewControllerDelegate <NSObject>
@optional

- (void)reloadDataWithMessage:(NSString *)message;

@end


@interface PayViewController : UIViewController

@property(strong, nonatomic) NSDictionary *dbUser;
@property (nonatomic, weak) id <PayViewControllerDelegate> delegate;

@end

NS_ASSUME_NONNULL_END
