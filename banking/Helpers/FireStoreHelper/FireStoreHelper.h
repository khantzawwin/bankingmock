//
//  FireStoreHelper.h
//  banking
//
//  Created by Khant Zaw Win on 16/5/21.
//

#import <Foundation/Foundation.h>
#import <FirebaseFirestore.h>

NS_ASSUME_NONNULL_BEGIN
@protocol FireStoreHelperDelegate <NSObject>
@optional

- (void)addOrUpdateUserSuccess;
- (void)getUserInfo: (NSDictionary *)userInfo;
- (void)getUsers: (NSArray *)users;
- (void)transactionSuccess;
- (void)errorWithMessage:(NSError *) message;

@end

@interface FireStoreHelper : NSObject

@property (strong, nonatomic) FIRFirestore *defaultDB;

+ (FireStoreHelper *)sharedInstance;
- (void)addOrUpdateUserWithEmail: (NSString *)email user:(NSDictionary *) user andDelegate: (id<FireStoreHelperDelegate>)delegate;
- (void)getUserByEmail: (NSString *)email withDelegate:(id<FireStoreHelperDelegate>)delegate;
- (void) makeTransactionWithSender:(NSDictionary *) sender AndRecipient:(NSDictionary *)recipient AndDelegate:(id<FireStoreHelperDelegate>)delegate;
- (void)getUserswithDelegate:(id<FireStoreHelperDelegate>)delegate;

@end

NS_ASSUME_NONNULL_END
