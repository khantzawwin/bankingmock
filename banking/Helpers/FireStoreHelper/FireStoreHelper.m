//
//  FireStoreHelper.m
//  banking
//
//  Created by Khant Zaw Win on 16/5/21.
//

#import "FireStoreHelper.h"

@implementation FireStoreHelper

+ (FireStoreHelper *)sharedInstance
{
    static FireStoreHelper* shared = nil;
    static dispatch_once_t pred;
    dispatch_once(&pred, ^{
        shared = [[self alloc] init];
        shared.defaultDB = [FIRFirestore firestore];
    });
    return shared;
}

- (void)addOrUpdateUserWithEmail: (NSString *)email user:(NSDictionary *) user andDelegate: (id<FireStoreHelperDelegate>)delegate {
    
    [[[self.defaultDB collectionWithPath:@"users"] documentWithPath:email] setData:user merge:YES
     completion:^(NSError * _Nullable error) {
      if (error != nil) {
        if ([delegate respondsToSelector:@selector(errorWithMessage:)])
            [delegate errorWithMessage:error];
      } else {
        if ([delegate respondsToSelector:@selector(addOrUpdateUserSuccess)])
            [delegate addOrUpdateUserSuccess];
      }
    }];
}

- (void)getUserByEmail: (NSString *)email withDelegate:(id<FireStoreHelperDelegate>)delegate {
    FIRDocumentReference *docRef = [[self.defaultDB collectionWithPath:@"users"] documentWithPath:email];

    [docRef getDocumentWithCompletion:^(FIRDocumentSnapshot *snapshot, NSError *error) {
      if (snapshot != NULL) {
          if ([delegate respondsToSelector:@selector(getUserInfo:)])
              [delegate getUserInfo: snapshot.data];
      } else {
         if ([delegate respondsToSelector:@selector(errorWithMessage:)])
             [delegate errorWithMessage:error];
      }
    }];
}

- (void)getUserswithDelegate:(id<FireStoreHelperDelegate>)delegate {;

    [[self.defaultDB collectionWithPath:@"users"] getDocumentsWithCompletion:^(FIRQuerySnapshot * _Nullable snapshot, NSError * _Nullable error) {
      if (snapshot != NULL) {
          if ([delegate respondsToSelector:@selector(getUsers:)])
              [delegate getUsers: snapshot.documents];
      } else {
         if ([delegate respondsToSelector:@selector(errorWithMessage:)])
             [delegate errorWithMessage:error];
      }
    }];
}

- (void) makeTransactionWithSender:(NSDictionary *) sender AndRecipient:(NSDictionary *)recipient AndDelegate:(id<FireStoreHelperDelegate>)delegate{
    FIRWriteBatch *batch = [self.defaultDB batch];
    
    FIRDocumentReference *sender_ref =
        [[self.defaultDB collectionWithPath:@"users"] documentWithPath:sender[@"email"]];
    [batch setData:sender forDocument:sender_ref];
    
    FIRDocumentReference *recipient_ref =
        [[self.defaultDB collectionWithPath:@"users"] documentWithPath:recipient[@"email"]];
    [batch setData:recipient forDocument:recipient_ref];
    
    [batch commitWithCompletion:^(NSError * _Nullable error) {
      if (error != nil) {
          if ([delegate respondsToSelector:@selector(errorWithMessage:)])
              [delegate errorWithMessage:error];
      } else {
          if ([delegate respondsToSelector:@selector(transactionSuccess)])
              [delegate transactionSuccess];
      }
    }];
}



@end
