//
//  Helper.h
//  banking
//
//  Created by Khant Zaw Win on 16/5/21.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface Helper : NSObject

+ (void)ShowErrorMsgDialogAlert:(NSString *)msg andView:(UIViewController *)controller;

@end

NS_ASSUME_NONNULL_END
