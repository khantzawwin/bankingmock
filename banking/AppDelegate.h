//
//  AppDelegate.h
//  banking
//
//  Created by Khant Zaw Win on 15/5/21.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>


@property (assign, nonatomic) NSString *setUserName;

+ (AppDelegate *)instance;

@end

